package inheritance;

public class Bookstore {
    
    public static void main(String[] args) {
        Book[] books = new Book[5];
        books[0] = new Book("Book1", "Author1");
        books[1] = new ElectronicBook("EBook1", "Author2", 300);
        books[2] = new Book("Book2", "Author3");
        books[3] = new ElectronicBook("EBook2", "Author4", 500);
        books[4] = new ElectronicBook("EBook3", "Author5", 500);
        for(Book i: books) {
            System.out.println(i);
        }
    }

}
